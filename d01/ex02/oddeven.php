#!/usr/bin/php
<?php

echo "Enter a number: ";
while($f = fgets(STDIN)){
	if (is_numeric(trim($f)))
	{
		echo "The number " . trim($f) . " is ";
		echo ($f % 2) ? ("odd\n") : ("even\n");
	}
	else
	{
		echo "'" . trim($f) . "' is not a number\n";
	}
	echo "Enter a number: ";
}
echo "\n";
