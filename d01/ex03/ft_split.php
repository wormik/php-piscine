<?php
function ft_split($str)
{
	$str = trim($str);
	$parts = preg_split('/\s+/', $str);
	sort($parts);
	return ($parts);
}
