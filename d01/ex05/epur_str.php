#!/usr/bin/php
<?php

if (count($argv) == 2)
{
	$str = trim($argv[1]);
	$pattern = '/\s+/i';
	$replace = ' ';
	echo preg_replace($pattern, $replace, $str) . "\n";
}
