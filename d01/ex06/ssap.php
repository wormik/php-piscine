#!/usr/bin/php
<?php

function	epur_str($str)
{
	$str = trim($str);
	$pattern = '/\s+/i';
	$replace = ' ';
	return (preg_replace($pattern, $replace, $str));
}

$res = array();

for ($i=1; $i < count($argv); $i++) {
	$temp = explode(' ', epur_str($argv[$i]));
	foreach ($temp as $elem) {
		array_push($res, $elem);
	}
}
sort($res);
foreach ($res as $elem) {
	echo $elem . "\n";
}
