#!/usr/bin/php
<?php

function	epur_str($str)
{
	$str = trim($str);
	$pattern = '/\s+/i';
	$replace = ' ';
	return (preg_replace($pattern, $replace, $str));
}

$res = array();

if (count($argv) > 1) {
	$temp = explode(' ', epur_str($argv[1]));
	foreach ($temp as $elem) {
		array_push($res, $elem);
	}
}
for ($i=1; $i < count($res); $i++) {
	echo $res[$i] . " ";
}
if (isset($res[0])) {
	echo $res[0] . "\n";
}
