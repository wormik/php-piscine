#!/usr/bin/php
<?php

function ft_is_sort($tab) {
	$temp = $tab;
	sort($tab);
	$diff = array_diff_assoc($temp, $tab);
	if (count($diff) == 0)
		return (1);
	else {
		rsort($tab);
		$diff = array_diff_assoc($temp, $tab);
		if (count($diff) == 0)
			return (1);
		return (0);
	}
}
