#!/usr/bin/php
<?php

function cmp2($a, $b)
{
	$max = max(strlen($a), strlen($b));
	for ($i=0; $i < $max; $i++) {
		if ($i == strlen($a)) {
			return (-1);
		}
		else if ($i == strlen($b)) {
			return (1);
		}
		$c1 = substr($a, $i, 1);
		$c2 = substr($b, $i, 1);
		if (strtolower($c1) == strtolower($c2))
		{
			continue ;
		}
		if (ctype_alpha($c1)) {
			if (ctype_alpha($c2)) {
				return (strtolower($c1) < strtolower($c2)) ? (-1) : (1);
			}
			else {
				return (-1);
			}
		}
		else if (is_numeric($c1)) {
			if (ctype_alpha($c2)) {
				return (1);
			}
			else if (is_numeric($c2)) {
				return ($c1 < $c2) ? (-1) : (1);
			}
			else {
				return (-1);
			}
		}
		else {
			if (ctype_alpha($c2) || is_numeric($c2)) {
				return (1);
			}
			else {
				return ($c1 < $c2) ? (-1) : (1);
			}
		}
	}
}

$res = array();
for ($i=1; $i < count($argv); $i++) {
	$temp = preg_split("/\s+/", $argv[$i]);
	foreach ($temp as $elem) {
		$res[] = $elem;
	}
}
usort($res, 'cmp2');
foreach ($res as $elem) {
	echo $elem . "\n";
}
