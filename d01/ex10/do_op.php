#!/usr/bin/php
<?php

if ($argc == 4)
{
	$a = trim($argv[1]);
	$b = trim($argv[3]);
	$op = trim($argv[2]);
	if ($op == '+') {
		echo $a + $b;
	}
	else if ($op == '-') {
		echo $a - $b;
	}
	else if ($op == '*') {
		echo $a * $b;
	}
	else if ($op == '/') {
		echo $a / $b;
	}
	else if ($op == '%') {
		echo $a % $b;
	}
	else {
		echo "Incorrect parameters";
	}
}
else {
	echo "Incorrect parameters";
}
echo "\n";