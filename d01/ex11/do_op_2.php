#!/usr/bin/php
<?php

if ($argc == 2)
{
	$str = trim($argv[1]);
	if (!preg_match('/^\-?\d+\s*[\+\-\*\/\%]\s*\-?\d+$/', $str)) {
		echo "Syntax Error";
	}
	else {
		sscanf($str, "%d %c %d", $a, $op, $b);
		if ($op == '+') {
			echo $a + $b;
		}
		else if ($op == '-') {
			echo $a - $b;
		}
		else if ($op == '*') {
			echo $a * $b;
		}
		else if ($op == '/') {
			echo $a / $b;
		}
		else if ($op == '%') {
			echo $a % $b;
		}
		else {
			echo "Lol error";
		}
	}
}
else {
	echo "Incorrect parameters";
}
echo "\n";