#!/usr/bin/php
<?php

if ($argc > 2) {
	$res = array();
	$it = $argv[1];
	for ($i=2; $i < $argc; $i++) {
		$pos = strpos($argv[$i], ':');
		$key = substr($argv[$i], 0, $pos);
		$value = substr($argv[$i], $pos + 1);
		$res[$key] = $value;
	}
	foreach ($res as $key => $value) {
		if (!strcmp($it, $key)) {
			echo "$value\n";
		}
	}
}