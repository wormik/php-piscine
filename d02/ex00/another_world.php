#!/usr/bin/php
<?php

if ($argc > 1)
{
	$str = trim($argv[1]);
	$pattern = '/\s+/i';
	$replace = ' ';
	echo preg_replace($pattern, $replace, $str) . "\n";
}
