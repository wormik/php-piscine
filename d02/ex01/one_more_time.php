#!/usr/bin/php
<?php

if ($argc == 2)
{
	$str = $argv[1];
	$str = preg_replace('/[Ll]undi/', "Monday", $str);
	$str = preg_replace('/[Mm]ardi/', "Tuesday", $str);
	$str = preg_replace('/[Mm]ercredi/', "Wednesday", $str);
	$str = preg_replace('/[Jj]eudi/', "Thursday", $str);
	$str = preg_replace('/[Vv]endredi/', "Friday", $str);
	$str = preg_replace('/[Ss]amedi/', "Saturday", $str);
	$str = preg_replace('/[Dd]imanche/', "Sunday", $str);

	$str = preg_replace('/[Jj]aniver/', "January", $str);
	$str = preg_replace('/[Ff]évrier/', "February", $str);
	$str = preg_replace('/[Mm]ars/', "March", $str);
	$str = preg_replace('/[Aa]vril/', "April", $str);
	$str = preg_replace('/[Mm]ai/', "May", $str);
	$str = preg_replace('/[Jj]uin/', "June", $str);
	$str = preg_replace('/[Jj]uillet/', "July", $str);
	$str = preg_replace('/[Aa]oût/', "August", $str);
	$str = preg_replace('/[Ss]eptembre/', "September", $str);
	$str = preg_replace('/[Oo]ctobre/', "October", $str);
	$str = preg_replace('/[Nn]ovembre/', "November", $str);
	$str = preg_replace('/[Dd]écembre/', "December", $str);

	date_default_timezone_set("Europe/Paris");

	if ($str != 0)
		echo strtotime($str), "\n";
	else
		echo "Wrong Format\n";
}
