#!/usr/bin/php
<?php
if ($argc != 2)
	exit ;
$html = file_get_contents($argv[1]);
$dom = new DOMDocument();
$dom->loadHTML($html, LIBXML_HTML_NODEFDTD);
$elements = $dom->getElementsByTagName('*');
foreach($elements as $node) {
	if ($node->hasAttribute('title')) {
		$new = $node->getAttribute('title');
		$new = strtoupper($new);
		$node->setAttribute('title', $new);
	}
	if ($node->nodeName == 'a') {
		$node->firstChild->nodeValue = strtoupper($node->firstChild->nodeValue);
	}
}
echo preg_replace('/<br>/', '<br />', $dom->saveHTML());
