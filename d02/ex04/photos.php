#!/usr/bin/php
<?php
if ($argc != 2)
	exit ;
libxml_use_internal_errors(true);
$url = $argv[1];
$html = file_get_contents($url);
$dom = new DOMDocument();
$dom->loadHTML($html);

$dest_path = preg_replace('/https?:\/\//', './', $url) . "/";
if (!file_exists($dest_path))
	mkdir($dest_path);
$elements = $dom->getElementsByTagName('img');
foreach($elements as $node) {
	if($node->hasAttribute('src')) {
		$from = $fullname = $node->getAttribute('src');
		$name = end(explode('/', $fullname));
		if (!strstr($from, $url)) {
			$from = $url."/".$from;
		}
		echo "$from, $dest_path$name\n";
		copy($from,  $dest_path . $name);
	}
}
