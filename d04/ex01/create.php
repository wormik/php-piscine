<?php
$submit = $_POST['submit'];
$login = $_POST['login'];
$passwd = hash('whirlpool', $_POST['passwd']);
if ($submit === 'OK' && $_POST['passwd'] && $login !== '' && $_POST['passwd'] && $login) {
	if (file_exists('../private/passwd')) {
		$file = file_get_contents('../private/passwd');
		$file = unserialize($file);
		if ($file) {
			foreach ($file as $row) {
				if ($row['login'] === $login) {
					echo "ERROR\n";
					exit ;
				}
			}
		}
		$file[] = array('login' => $login, 'passwd' => $passwd);
		$file = serialize($file);
		file_put_contents('../private/passwd', $file);
		echo "OK\n";
	}
	else  {
		if (!file_exists('../private')) {
			mkdir('../private', 0777);
		}
		$row[] = array('login' => $login, 'passwd' => $passwd);
		$row = serialize($row);
		file_put_contents('../private/passwd', $row);
		echo "OK\n";
	}
}
else {
	echo "ERROR\n";
}
?>
