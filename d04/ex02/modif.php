<?php
$submit = $_POST['submit'];
$login = $_POST['login'];
$oldpw = hash('whirlpool', $_POST['oldpw']);
$newpw = hash('whirlpool', $_POST['newpw']);
if ($submit === 'OK' && $_POST['oldpw'] !== '' && $_POST['oldpw'] && $login !== '' && $login
	&& $_POST['newpw'] !== '' && file_exists('../private/passwd')) {
	$found = FALSE;
	$file = file_get_contents('../private/passwd');
	$file = unserialize($file);
	if ($file) {
		foreach ($file as $key => $row) {
			if ($row['login'] === $login && $row['passwd'] === $oldpw) {
				$found = TRUE;
				$file[$key]['passwd'] = $newpw;
			}
		}
	}
	if (!$found) {
		echo "ERROR\n";
		exit ;
	}
	$file = serialize($file);
	file_put_contents('../private/passwd', $file);
	echo "OK\n";
}
else {
	echo "ERROR\n";
}
?>
