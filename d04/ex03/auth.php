<?php
function auth($login, $passwd) {
	$passwd = hash('whirlpool', $passwd);
	if (file_exists('../private/passwd')) {
		$file = file_get_contents('../private/passwd');
		$file = unserialize($file);
		if ($file) {
			foreach ($file as $row) {
				if ($row['login'] === $login && $row['passwd'] === $passwd) {
					return (TRUE);
				}
			}
		}
	}
	return (FALSE);
}
