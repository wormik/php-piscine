<?php

/**
 * Created by PhpStorm.
 * User: WormiK
 * Date: 4/12/2017Q
 * Time: 12:42 PM
 */
class Color
{
    public $red;
    public $green;
    public $blue;
    static $verbose = FALSE;

    public function __construct($array)
    {
        if (array_key_exists('rgb', $array)) {
            $color_val = intval($array['rgb']);
            $this->red = 0xFF & ($color_val >> 0x10);
            $this->green = 0xFF & ($color_val >> 0x8);
            $this->blue = 0xFF & $color_val;
        }
        else if (array_key_exists('red', $array)
            && array_key_exists('green', $array)
            && array_key_exists('blue', $array)) {
            $this->red = intval($array['red']);
            $this->green = intval($array['green']);
            $this->blue = intval($array['blue']);
        }
        else {
            $this->red = 0;
            $this->green = 0;
            $this->blue = 0;
        }
        if ($this->red < 0)
            $this->red = 0;
        if ($this->green < 0)
            $this->green = 0;
        if ($this->blue < 0)
            $this->blue = 0;
        if ($this->red > 255)
            $this->red = 255;
        if ($this->green > 255)
            $this->green = 255;
        if ($this->blue > 255)
            $this->blue = 255;
        if (self::$verbose === True) {
            print($this . ' constructed.' . PHP_EOL);
        }
    }

    function __destruct() {
        if (self::$verbose === True)
            print($this . ' destructed.' . PHP_EOL);
    }

    public function add($that) {
        return new Color(array(
            'red' => $this->red + $that->red,
            'green' => $this->green + $that->green,
            'blue' => $this->blue + $that->blue,
        ));
    }

    public function sub($that) {
        return new Color(array(
            'red' => $this->red - $that->red,
            'green' => $this->green - $that->green,
            'blue' => $this->blue - $that->blue,
        ));
    }

    public function mult($scale) {
        return new Color(array(
            'red' => $this->red * $scale,
            'green' => $this->green * $scale,
            'blue' => $this->blue * $scale,
        ));
    }

    static function doc() {
        return (file_get_contents('Color.doc.txt'));
    }

    function __toString() {
        return(
            'Color( red: ' . sprintf("%3s", $this->red)
            . ', green: ' . sprintf("%3s", $this->green)
            . ', blue: '. sprintf("%3s", $this->blue)
            .' )'
        );
    }


}