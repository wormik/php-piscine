<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/12/17
 * Time: 7:55 PM
 */
class Vector
{
    private $_x;
    private $_y;
    private $_z;
    private $_w;
    public static $verbose = FALSE;

    public function __construct($array)
    {
        if ($array['dest'] !== NULL) {
            $x0 = 0.0;
            $y0 = 0.0;
            $z0 = 0.0;
            $w0 = 1.0;
            if ($array['orig'] !== NULL) {
                $orig = $array['orig'];
                $x0 = $orig->getX();
                $y0 = $orig->getY();
                $z0 = $orig->getZ();
                $w0 = $orig->getW();
            }
            $dest = $array['dest'];
            $this->_x = $dest->getX() - $x0;
            $this->_y = $dest->getY() - $y0;
            $this->_z = $dest->getZ() - $z0;
            $this->_w = 0.0;
            if (self::$verbose === TRUE) {
                print($this . ' constructed' . PHP_EOL);
            }
        }
        else {
            exit ("Mandatory 'dest' value omitted\n");
        }
    }


    public function getX()
    {
        return $this->_x;
    }

    public function getY()
    {
        return $this->_y;
    }

    public function getZ()
    {
        return $this->_z;
    }

    public function getW()
    {
        return $this->_w;
    }

    static function doc() {
        return (file_get_contents('Vertex.doc.txt'));
    }

    function __toString() {
        return(
            'Vertex( x:' . sprintf("%3.2f", $this->_x)
            . ', y:' . sprintf("%3.2f", $this->_y)
            . ', z:' . sprintf("%3.2f", $this->_z)
            . ', w:' . sprintf("%3.2f", $this->_w)
            . ' )'
        );
    }
}