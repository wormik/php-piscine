<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/12/17
 * Time: 6:20 PM
 */

require_once 'Color.class.php';
class Vertex
{
    private $_x;
    private $_y;
    private $_z;
    private $_w = 1.0;
    private $_color;
    public static $verbose = FALSE;

    public function __construct($array)
    {
        if ($array['x'] !== NULL && $array['z'] !== NULL && $array['z'] !== NULL) {
            $this->_x = $array['x'];
            $this->_y = $array['y'];
            $this->_z = $array['z'];
            if ($array['w'] !== NULL) {
                $this->_w = $array['w'];
            }
            if ($array['color'] !== NULL) {
                $this->_color = clone $array['color'];
            }
            else {
                $this->_color = new Color(array('rgb' => 16777215));
            }
            if (self::$verbose === TRUE) {
                print($this . ' constructed' . PHP_EOL);
            }
        }
        else {
            exit("Mandatory values 'x', 'y', 'z' are omitted\n");
        }
    }

    function __destruct()
    {
        if (self::$verbose === TRUE) {
            print($this . ' destructed' . PHP_EOL);
        }
    }

    static function doc() {
        return (file_get_contents('Vertex.doc.txt'));
    }

    public function getX()
    {
        return $this->_x;
    }

    public function setX($x)
    {
        $this->_x = $x;
    }

    public function getY()
    {
        return $this->_y;
    }

    public function setY($y)
    {
        $this->_y = $y;
    }

    public function getZ()
    {
        return $this->_z;
    }

    public function setZ($z)
    {
        $this->_z = $z;
    }

    public function getW()
    {
        return $this->_w;
    }

    public function setW($w)
    {
        $this->_w = $w;
    }

    public function getColor()
    {
        return $this->_color;
    }

    public function setColor($color)
    {
        $this->_color = clone $color;
    }

    function __toString() {
        return(
            'Vertex( x: ' . sprintf("%3.2f", $this->_x)
            . ', y: ' . sprintf("%3.2f", $this->_y)
            . ', z:' . sprintf("%3.2f", $this->_z)
            . ', w:' . sprintf("%3.2f", $this->_w)
            . ((self::$verbose) ? (', ' . $this->_color) : ('')) . ' )'
        );
    }

}