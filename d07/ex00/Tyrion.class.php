<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/13/17
 * Time: 1:15 PM
 */

class Tyrion extends Lannister
{
    public function __construct()
    {
        parent::__construct();
        print("My name is Tyrion" . PHP_EOL);
    }
    public function getSize() {
        return "Short";
    }
}

