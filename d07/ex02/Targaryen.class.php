<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/13/17
 * Time: 2:11 PM
 */
class Targaryen
{
    public function resistsFire() {
        return (FALSE);
    }
    public function getBurned() {
        if ($this->resistsFire()) {
            return ('emerges naked but unharmed');
        }
        else {
            return ('burns alive');
        }
    }
}