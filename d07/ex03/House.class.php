<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/13/17
 * Time: 2:25 PM
 */
abstract class House
{
    abstract public function getHouseName();
    abstract public function getHouseMotto();
    abstract public function getHouseSeat();
    public function introduce() {
        echo 'House ' . $this->getHouseName()
            . ' of ' . $this->getHouseSeat()
            . ' : "' . $this->getHouseMotto()
            . '"' . PHP_EOL;
    }
}