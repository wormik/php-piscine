<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/13/17
 * Time: 2:35 PM
 */
class Jaime extends Lannister
{
    public function sleepWith($b) {
        $class = get_class($b);
        if ($class === 'Tyrion') {
            echo "Not even if I'm drunk !" . PHP_EOL;
        }
        else if ($class === 'Sansa') {
            echo "Let's do this." . PHP_EOL;
        }
        else if ($class === 'Cersei') {
            echo "With pleasure, but only in a tower in Winterfell, then." . PHP_EOL;
        }
    }
}