<?php

/**
 * Created by PhpStorm.
 * User: abykov
 * Date: 4/13/17
 * Time: 1:15 PM
 */

class Tyrion extends Lannister
{
    public function sleepWith($b) {
        $class = get_class($b);
        if ($class === 'Jaime') {
            echo "Not even if I'm drunk !" . PHP_EOL;
        }
        else if ($class === 'Sansa') {
            echo "Let's do this." . PHP_EOL;
        }
        else if ($class === 'Cersei') {
            echo "Not even if I'm drunk !" . PHP_EOL;
        }
    }
}

